@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains('user_id', Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif

            <!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
			  Post Comment
			</button>

            <div class="mt-3">
                <a href="/posts" class="card-link">View All Posts</a>
            </div>
        </div>
    </div>

    <h2 class="card-title">Comments:</h2>
            {{-- <p class="text-center">{{$post_comments->content}}</p> --}}
						

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <form method="POST" action="/posts/{{$post->id}}/comment">
		@method('PUT')
		{{-- Cross-Site Request Forgery --}}
		@csrf
		
		<div class="form-group">
			<label for="content">Comment:</label>
			<textarea class="form-control" id="content" name="content" row="3"></textarea>
		</div>
		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Post Comment</button>
		</div>
	</form>
      </div>
    </div>
  </div>
</div>
@endsection