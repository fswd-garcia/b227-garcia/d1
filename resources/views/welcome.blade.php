@extends('layouts.app')

@section('content')
    <div class="card text-center ">
        <div class="card-body">
        <img src="https://laravelnews.imgix.net/images/laravel-featured.png" alt="" class="w-50">
        </div>
    </div>
    <div class="card text-center">
        <div class="card-body mt-3">
        <h3>Featured Posts</h3>
        </div>        
    </div>
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="card text-center">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                   
                </div>
               
            </div>
        @endforeach
    @else
        <div>
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class="btn btn-info">Create post</a>
        </div>
    @endif
@endsection