<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via the Auth facedes
use Illuminate\Support\Facades\Auth; 
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // action to return a view containing a form for a blog post creation.

    public function create(){
        return view('posts.create');
    }

    //action to receive form data and subsequently store said data in the posts table.
    public function store(Request $request){
       if(Auth::user()){
         $post = new Post;
         $post->title = $request->input('title');
         $post->content = $request->input('content');
         $post->user_id = (Auth::user()->id);

         $post->save();

         return redirect('/posts');
    } else{
        return redirect('/login');
    }
    }

    //action that will return a view showing all the blog posts.
    public function index(){
        //$posts = Post::all();
        // return view('posts.index')->with('posts', $posts);
        //$users = DB::table('users')->where('votes', 100)->get();
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }

    //Activity for s02
    //action that will return a view showing random blog posts
    public function welcome()
    {
        //$posts = Post::all();
        $posts = Post::inRandomOrder()
                ->where('isActive', true)
                ->limit(3)
                ->get();

        return view('welcome')->with('posts', $posts);


    }

    // action for showing the posts authored by authenticated user
    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else {
            return redirect('/login');
        }
    }

    //action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    // Activity 3
    // action that will return an edit form for a specific Post when a GET request is received at the /posts/{id}/edit endpoint
    public function edit($id){
        if(Auth::user()){
           $post = Post::find($id);
            return view('posts.edit')->with('post', $post); 
        }
        else {
            return redirect('/login');
        }
        
    }


    public function update(Request $request, $id){
        $post = Post::find($id);
        // if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        } 
        
            return redirect('/posts');
        
    }

    public function destroy($id){
        $post = Post::find($id);

        //if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }

        return redirect('/posts');
    }

    public function archive($id){
        $post = Post::find($id);
        //if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            //$post->delete();
            $post->save();
        }

        return redirect('/posts');
    }

    public function unarchive($id){
        $post = Post::find($id);
        //if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->isActive = true;
            //$post->delete();
            $post->save();
        }

        return redirect('/posts');
    }

    // action that will allow a authenticated user who is not the post author to toggle the like on the post being view.
    public function likes($id){
        $post = Post::find($id);
        //To check if there is a logged in user
        if(Auth::user()){
            $user_id = Auth::user()->id;
            //if authrneticated user is not the post author
            if($post->user_id != $user_id){
                //check if a post like has been made
                if($post->likes->contains("user_id", $user_id)){
                    //deletes the like made by the user to unlike this post
                    PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();

                }
                //create a new like record in the post_likes table with the user_id and post_id
                else {
                    //create a new like record to like this post
                    //instantiate a new PostLike object from the PostLike model
                    $postLike = new PostLike;
                    //define the properties of the $postLike object
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id; 

                    //save the postlike object in the database
                    $postLike->save();
                }

                return redirect("/posts/$id");
            }


        }
        else {
            return redirect('/login');
        }
    }

    public function comment(Request $request, $id){
        $post_comments = Post::find($id);
        
        
            $post_comments->content = $request->input('content');
            $post_comments->save();

            
            return redirect('/posts');
    }
}
